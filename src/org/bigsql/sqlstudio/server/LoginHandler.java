/*
 * SQL Studio
 * 
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 * 
 */

package org.bigsql.sqlstudio.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bigsql.sqlstudio.server.connection.ConnectionInfo;
import org.bigsql.sqlstudio.server.connection.ConnectionManager;
import org.bigsql.sqlstudio.shared.DatabaseConnectionException;

public class LoginHandler extends HttpServlet
{

	private static final long serialVersionUID = -3598253864439571563L;

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{

		String databaseType, dbHost = null, dbName = null, username = null, passwd, clientIP, userAgent, queryString = null, token = null;
		HttpSession session = null;
		int dbPort = -1;

		try
		{
			databaseType = req.getParameter("database-type");

			clientIP = ConnectionInfo.remoteAddr(req);
			userAgent = req.getHeader("User-Agent");
			queryString = req.getQueryString();

			session = req.getSession(true);
			session.setAttribute("database-type", databaseType);

			dbHost = req.getParameter("dbURL");
			dbPort = Integer.parseInt(req.getParameter("dbPort"));
			dbName = req.getParameter("dbName");
			username = req.getParameter("username");
			passwd = req.getParameter("password");

			token = ConnectionManager.addConnection(req, dbHost, dbPort,
					dbName, username, passwd, clientIP, userAgent);

			if (token != null && !token.equals(""))
			{
				session.setAttribute("dbToken", token);
				session.setAttribute("dbVersion", Integer
						.toString(ConnectionManager.getDatabaseVersion(req)));
			}
			
			/* Cleanup Error Message, upon login successful.*/
			session.removeAttribute("err_msg");

		}
		catch (DatabaseConnectionException e)
		{
			session.setAttribute("err_msg", e.getMessage());
			session.setAttribute("dbName", dbName);
			session.setAttribute("dbURL", dbHost);
			session.setAttribute("username", username);
		}

		// Try redirecting the client to the page he first tried to access
		try
		{
			String target = this.getServletContext().getInitParameter(
					"home_url");

			if (queryString != null)
				target = target + "?" + queryString;

			res.setContentType("text/html");
			res.sendRedirect(target);

			return;
		}
		catch (Exception ignored)
		{}

	}
}
