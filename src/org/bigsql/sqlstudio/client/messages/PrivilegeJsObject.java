/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.messages;

import com.google.gwt.core.client.JavaScriptObject;

public class PrivilegeJsObject extends JavaScriptObject {

	protected PrivilegeJsObject() {
    }


    public final native String getId() /*-{ return this.id}-*/;

    public final native String getGrantee() /*-{ return this.grantee }-*/;
    public final native String getType() /*-{ return this.type }-*/;
    public final native String getGrantable() /*-{ return this.grantable }-*/;
    public final native String getGrantor() /*-{ return this.grantor }-*/;

}
