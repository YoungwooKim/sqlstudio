/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.messages;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class ItemDataJsObject extends JavaScriptObject {

	protected ItemDataJsObject() {
    }

    public final native JsArray<QueryMetaDataJsObject> getMetaData() /*-{ return this.metadata }-*/;

    public final native JsArray<?> getResultSet() /*-{ return this.resultset }-*/;
}
