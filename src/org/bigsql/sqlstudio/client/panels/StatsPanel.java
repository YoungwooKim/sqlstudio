/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.panels;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import org.bigsql.sqlstudio.client.SqlStudio;
import org.bigsql.sqlstudio.client.models.ModelInfo;
import org.bigsql.sqlstudio.client.models.StatsInfo;
import org.bigsql.sqlstudio.client.providers.StatsListDataProvider;

public class StatsPanel extends Composite implements DetailsPanel {

	private static String MAIN_HEIGHT = "300px";

	private DataGrid<StatsInfo> dataGrid;
    private StatsListDataProvider dataProvider = new StatsListDataProvider();


	private static interface GetValue<C> {
	    C getValue(StatsInfo column);
	  }

	public void setItem(ModelInfo item) {
		dataProvider.setItem(item.getId(), item.getItemType());		
	}
	
	public StatsPanel() {
		
		VerticalPanel mainPanel = new VerticalPanel();

		mainPanel.add(getButtonBar());
		mainPanel.add(getMainPanel());
				
		initWidget(mainPanel);
	}

	private Widget getButtonBar() {
		HorizontalPanel bar = new HorizontalPanel();
		
		PushButton refresh = getRefreshButton();
		
		bar.add(refresh);
		
		return bar.asWidget();
	}
	
	private PushButton getRefreshButton() {
		PushButton button = new PushButton(new Image(SqlStudio.Images.refresh()));
		button.setTitle("Refresh");
		
		return button;
	}
	
	private Widget getMainPanel() {
		SimplePanel panel = new SimplePanel();
		panel.setWidth("100%");
		panel.setHeight(MAIN_HEIGHT);
		
		dataGrid = new DataGrid<StatsInfo>(25, StatsInfo.KEY_PROVIDER);
		dataGrid.setHeight(MAIN_HEIGHT);
		
		Column<StatsInfo, String> name = addNameColumn(new TextCell(), "Name", new GetValue<String>() {
	        public String getValue(StatsInfo stat) {
	          return stat.getName();
	        }
	      }, null);

		Column<StatsInfo, String> value = addNameColumn(new TextCell(), "Value", new GetValue<String>() {
	        public String getValue(StatsInfo stat) {
	          return stat.getValue();
	        }
	      }, null);

		dataGrid.setLoadingIndicator(new Image(SqlStudio.Images.spinner()));
		dataProvider.addDataDisplay(dataGrid);

		panel.add(dataGrid);
		
		return panel.asWidget();
	}	
	
	private <C> Column<StatsInfo, C> addNameColumn(Cell<C> cell, String headerText,
		      final GetValue<C> getter, FieldUpdater<StatsInfo, C> fieldUpdater) {
		    Column<StatsInfo, C> column = new Column<StatsInfo, C>(cell) {
		      @Override
		      public C getValue(StatsInfo object) {
		        return getter.getValue(object);
		      }
		    };
		    column.setFieldUpdater(fieldUpdater);

		    dataGrid.addColumn(column, headerText);
		    return column;
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		
	}

}
